const colors = {
  Appetizers: '#C2185B', // pink
  Breakfast: '#7B1FA2', // purple
  Dessert: '#1976D2', // blue
  Sauces: '#009688', // teal
  Entrees: '#689F38', // light green
  Salad: '#F57C00', // orange
  Seasoning: '#D32F2F', // red
  Sides: '#455A64', // blue gray
  Drinks: '#795548', // brown
  Snacks: '#FFC107', // dark yellow
  Soup: '#6D6D6D', // dark gray
}

export default colors
